# Example files for <https://pintergabor.eu/en/hw/STM8S-blinky/>

Written by Pintér Gábor  
Feuerwehrgasse 1/2/9  
Purbach am Neusiedlersee  
A-7083, Austria  
Tel: +43 670 2055090  
Email: <pinter.gabor@gmx.at>  
Web: <https://pintergabor.eu>  

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

<https://www.gnu.org/copyleft/gpl.html>

For commercial license, and for custom HW, FW and SW please contact the author.

## simple - Simple blinky
Just a simple RED/GREEN/BLUE blinky for the first tests.

## standard - Standard command set
Communication format is 115200 baud, no parity, one stop bit. Commands start with a command letter, and end with '\n'.

* Rxx  
  Set red led intensity. xx is hexadecimal. 00=off, FF=full on.

* Gxx  
  Set green led intensity. xx is hexadecimal. 00=off, FF=full on.

* Bxx  
  Set blue led intensity. xx is hexadecimal. 00=off, FF=full on.

* Pnxxxx  
  Set PWM for servo control. n= 0..1. xxxx= high time in milliseconds. Hexadecimal. 3E8= left position, 5DC= mid position, 7D0=right position.

* UUU  
  Identify. And later may be used for automatic baud rate detection. Please send it first after power on.

* V  
  Get version number

* Sxx  
  Execute test command after power on.  
  The host must wait for the answer before sending another command.

* Txx  
  Execute test command. Currently known commands:
  * 0 - Communication test  
  * 1 - LED test  
  * 2 - PWM test  
  * 3 - LED test 2  

## multicolor - 281,474,976,710,656 colors
The same as standard, but R, G, B commands have four digit hexadecimal parameters.

## dev - It is not four you
My development branch. Please do not touch.
